#!/bin/bash
if [ "$#" -ne 3 ]; then
  echo "Usage: $0 <KAFINEA_OFFLINE_LOGIN> <KAFINEA_OFFLINE_PASSWORD> <KAFINEA_BIRT_PASSWORD>"
  exit
fi

export KAFINA_OFFLINE_USER=$1
export KAFINA_OFFLINE_PASSWORD=$2
export KAFINA_BIRT_PASSWORD=$3

echo                                                                                                                                                                                 
echo "- Force stopping current kafinea instance..."                                                                                              
echo 
sudo stop-kafinea

echo                                                                                                                                                                                
echo "- Getting latest src files..."                                                                                              
echo 
cd /tmp
rm -rf kafinea-offline
git clone "https://${KAFINA_OFFLINE_USER}:${KAFINA_OFFLINE_PASSWORD}@gitlab.com/madiasoft/kafinea-offline.git"
sudo mv /tmp/kafinea-offline/docker-compose.yaml.template /usr/local/kafinea/
sudo rm -rf /usr/local/kafinea/localimage
sudo rm -rf /usr/local/kafinea/bin
sudo mv /tmp/kafinea-offline/localimage /usr/local/kafinea/
sudo mv /tmp/kafinea-offline/bin /usr/local/kafinea/
sudo rm -rf /usr/local/kafinea/proftpd 
sudo mv /tmp/kafinea-offline/proftpd /usr/local/kafinea/
sudo rm -rf /tmp/kafinea-offline

sudo rm -f /tmp/lock.txt
sudo echo "UPDATE" > /usr/local/kafinea/conf/updatekafinea.txt

cd /usr/local
sudo update-kafinea $KAFINA_OFFLINE_USER $KAFINA_OFFLINE_PASSWORD $KAFINA_BIRT_PASSWORD
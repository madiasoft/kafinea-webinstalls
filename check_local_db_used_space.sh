#!/bin/bash
csv_file=local_db_storage_usage.csv

# Get all StatefulSets in the current namespace
statefulsets=$(kubectl get statefulsets -o jsonpath='{.items[*].metadata.name}' | tr ' ' '\n' | grep "\-local\-db")

# Loop through each StatefulSet
for sts in $statefulsets; do
  echo "StatefulSet: $sts"
  
  # Check if the StatefulSet has active replicas
  replicas=$(kubectl get statefulset $sts -o jsonpath='{.status.replicas}')
  if [ "$replicas" -gt 0 ]; then
    echo "  Active replicas: $replicas"
  else
    echo "  No active replicas. Scaling up to 1 replica..."
    kubectl scale statefulset $sts --replicas=1
    
    # Wait for the pod to be ready
    kubectl rollout status statefulset $sts
  fi
  
  # Get the pods for the current StatefulSet
  pods=$(kubectl get pods -l app=$sts -o jsonpath='{.items[*].metadata.name}')
  
  # Loop through each pod and print its name
  for pod in $pods; do
    echo "    Pod: $pod"
    name=$(echo $pod | cut -d'-' -f1)
    used_space=$(kubectl exec -it $pod -- df -h /var/lib/mysql | awk -F\  '{print $5}' | tail -n 1)
    total_space=$(kubectl exec -it $pod -- df -h /var/lib/mysql | awk -F\  '{print $2}' | tail -n 1)
    echo "$name;$total_space;$used_space" >> $csv_file
  done
done
#!/bin/bash
for i in $(kubectl get pv -o json | jq -j '.items[] | "\(.metadata.name),\(.spec.claimRef.name),\(.status.phase)\n"' | grep "kfn" | grep "Released"); do
  pvName=$(echo $i | cut -d, -f1)
  pvPath=$(echo $i | cut -d, -f2)
  echo "$pvName -> $pvPath"
  echo "Deleting NFS directory..."
  du -sh /current/*${pvPath}*
  rm -rf /current/*${pvPath}*
  echo "Deleting pv..."
  kubectl delete pv ${pvName}
done
